import styled from "styled-components";

export const InputSearch = styled("input")`{
  width: 100%;
  padding: 10px;
  border: 0;
  outline: none;
  font-size: 16px;
  margin-right: 10px;
`;

export const SearchPanelWrapper = styled("div")`
  display: flex;
  align-items: center;
  flex-flow: wrap;
  background: #FFFFFF 0 0 no-repeat padding-box;
  padding: 15px;
  border-radius: 10px;
`;


export const DistanceWrapper = styled("div")`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const Profile = styled("section")`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  //margin-top: 63px;
`;

export const Main = styled("div")`
  width: 100%;
  background: #F0F0F0 0% 0% no-repeat padding-box;
  padding: 15px;
`;

export const SearchBlock = styled("div")`
  //width: 345px;
  height: 47px;
  border-radius: 30px;
  border: 1px solid #0d9da6;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background: #fff;
  //margin-right: 100px;
  //padding: 0;
`;